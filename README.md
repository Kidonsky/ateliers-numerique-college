# Ateliers numerique collège

Dépot pour partager des ateliers sur l'utilisation du numérique, des nouvelles technologies de l'information, les réseaux sociaux, les problématiques de harcèlement et de données personnelles, les outils audio, video, photo...
Ces ateliers sont donnés dans un établissement scolaire à Annecy et sont à destination de collégiens volontaires.

Ils durent chacun environ une heure, rassemblent des collégiens de différents niveaux de la 5e à la 3e et les parents sont aussi invités.


Ateliers numérique pour collégiens © 2023-2024 par Jean-Luc Genêt et Bryan Pereira est sous licence CC BY-SA 4.0. Pour voir une copie de cette licence, voir http://creativecommons.org/licenses/by-sa/4.0/
